var express = require('express');
var mdAutenticacion = require('../middlewares/autenticacion');

var Estudio = require('../models/estudio');

var app = express();

app.get('/',( req, res ) => {
    Estudio.find({  })
    .exec(
        ( err, estudio ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando estudio',
                    errors: err
                });
            } 
            Estudio.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                estudio,
                total: conteo
            });
        });
    });
});

app.post('/', mdAutenticacion.verificaToken,( req, res ) => {
    var body = req.body;    
    var estudio = new Estudio({
        competition: body.competition,
        visibility: body.visibility,
        access: body.access,
        date: body.date,
        action: body.action,
        risk: body.risk,
        supervisor: body.supervisor,
        status: body.status,
        sign: body.sign,
        open: body.open,
        addToPortfolio: body.addToPortfolio,
        addToBestCases: body.addToBestCases,
        addToPipeline: body.addToPipeline,
    });
    
    estudio.save( ( err, estudioGuardado ) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al guardar estudio',
                err
            });
        }
        res.status(201).json({
            ok: true,
            estudioGuardado,
        });
    });
});

app.put('/:id', mdAutenticacion.verificaToken,( req, res ) => {
    var id = req.params.id;
    var body = req.body;

    Estudio.findById(id, (err, estudio) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar estudio',
                errors: err
            });
        }
    
        if (!estudio) {
            return res.status(400).json({
                ok: false,
                msj: 'la estudio con el id '+ id + ' no existe',
                errors: { message: 'No existe una estudio con ese ID' }
            });
        }

        estudio.competition = body.competition,
        estudio.visibility = body.visibility,
        estudio.access = body.access,
        estudio.date = body.date,
        estudio.action = body.action,
        estudio.risk = body.risk,
        estudio.supervisor = body.supervisor,
        estudio.status = body.status,
        estudio.sign = body.sign,
        estudio.open = body.open,
        estudio.addToPortfolio = body.addToPortfolio,
        estudio.addToBestCases = body.addToBestCases,
        estudio.addToPipeline = body.addToPipeline,

        estudio.save( (err, estudioGuardado) => {

            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar estudio',
                    errors: err
                });
            }

            res.status(201).json({
                ok: true,
                estudioGuardado
            });
        });

    });
});

app.delete('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE] ,( req, res ) => {
    var id = req.params.id;
    Estudio.findByIdAndRemove(id, (err, estudioBorrado) => { 
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar estudio',
                errors: err
            });
        }

        if( !estudioBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe estudio con ese id',
                errors: { message: 'No existe estudio con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            estudioBorrado
        });
    });
});

module.exports = app;