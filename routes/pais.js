var express = require('express');
var app = express();
var Pais = require('../models/pais');
var mdAutenticacion = require('../middlewares/autenticacion');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED

app.get('/',( req, res, next ) => {

    Pais.find({  })
    .populate( 'usuario', 'nombre email' )
    .exec(
        ( err, paises ) => {
            if( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error cargando basica',
                    errors: err
                });
            } 
            Pais.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                paises: paises,
                total: conteo
            });
        });
    });
});

app.post('/', mdAutenticacion.verificaToken,( req, res ) => {
    
    var body = req.body;
    var pais = new Pais({
        nombre: body.nombre,
        img: body.img,
        usuario: body.usuario,
    });

    pais.save( ( err, paisGuardado ) => {

        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pais',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            pais: paisGuardado,
        });
    });
});

//obtener pais

app.get('/:id', (req, res, next) =>{
    var id = req.params.id;
    Pais.findById( id )
    .populate('usuario', 'nombre ,email ,img')
    .exec( ( err, pais) =>{
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pais',
                errors: err
            });
        }

        if (!pais) {
            return res.status(400).json({
                ok: false,
                msj: 'El pais con el id '+ id + ' no existe',
                errors: { message: 'No existe un pais con ese ID' }
            });
        }

        return res.status(200).json({
            ok: true,
            pais: pais
        });
    })
});

app.put('/:id',mdAutenticacion.verificaToken,( req, res, next ) => {
    var id = req.params.id;
    var body = req.body;    
    
    Pais.findById(id, (err, pais) => {
        
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar pais',
                errors: err
            });
        }
    
        if (!pais) {
            return res.status(400).json({
                ok: false,
                msj: 'El pais con el id '+ id + ' no existe',
                errors: { message: 'No existe un pais con ese ID' }
            });
        }

        pais.nombre = body.nombre;
        pais.usuario = body.usuario;
        pais.img = body.img;
        
        pais.save( (err, paisGuardado) => {
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar pais',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                pais: paisGuardado
            });
        })
    });
});

app.delete('/:id', mdAutenticacion.verificaToken ,( req, res ) => {
    var id = req.params.id;

    Pais.findByIdAndRemove(id, (err, paisBorrado) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar pais',
                errors: err
            });
        }

        if( !paisBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe pais con ese id',
                errors: { message: 'No existe pais con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            usuario: paisBorrado
        });
    })
});

module.exports = app;