var express = require('express');
var mdAutenticacion = require('../middlewares/autenticacion');

var Localizacion = require('../models/localizacion');

var app = express();

app.get('/',( req, res ) => {
    Localizacion.find({  })
    .exec(
        ( err, localizacion ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando Localizacion',
                    errors: err
                });
            } 
            Localizacion.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                localizacion,
                total: conteo
            });
        });
    });
});

app.post('/', mdAutenticacion.verificaToken,( req, res ) => {
    var body = req.body;
    var localizacion = new Localizacion({
        public: body.public,
        building: body.building,
        private: body.private,
        concurso: body.concurso,
        bilateral: body.bilateral,
        plot: body.plot,
        demolition: body.demolition,
        urbanized: body.urbanized,
        sizePlot: body.sizePlot,
        sizeBuilding: body.sizeBuilding,
        volume: body.volume,
        walkTo: body.walkTo,
        parking: body.parking,
        speaker: body.speaker,
        driveTo: body.driveTo,
        direccionMapa: body.direccionMapa,
    });
    
    localizacion.save( ( err, localizacionGuardado ) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al guardar localizacion',
                err
            });
        }
        res.status(201).json({
            ok: true,
            localizacion,
        });
    });
});

app.put('/:id', mdAutenticacion.verificaToken,( req, res ) => {
    var id = req.params.id;
    var body = req.body;

    Localizacion.findById(id, (err, localizacion) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar Localizacion',
                errors: err
            });
        }
    
        if (!localizacion) {
            return res.status(400).json({
                ok: false,
                msj: 'la Localizacion con el id '+ id + ' no existe',
                errors: { message: 'No existe una Localizacion con ese ID' }
            });
        }

        localizacion.public = body.public,
        localizacion.building = body.building,
        localizacion.private = body.private,
        localizacion.concurso = body.concurso,
        localizacion.bilateral = body.bilateral,
        localizacion.plot = body.plot,
        localizacion.demolition = body.demolition,
        localizacion.urbanized = body.urbanized,
        localizacion.sizePlot = body.sizePlot,
        localizacion.sizeBuilding = body.sizeBuilding,
        localizacion.volume = body.volume,
        localizacion.walkTo = body.walkTo,
        localizacion.parking = body.parking,
        localizacion.speaker = body.speaker,
        localizacion.driveTo = body.driveTo,
        localizacion.direccionMapa = body.direccionMapa,

        localizacion.save( (err, localizacionGuardado) => {

            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar localizacion',
                    errors: err
                });
            }

            res.status(201).json({
                ok: true,
                localizacionGuardado
            });
        });

    });
});

app.delete('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE] ,( req, res ) => {
    var id = req.params.id;
    Localizacion.findByIdAndRemove(id, (err, localizacionBorrado) => { 
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar localizacion',
                errors: err
            });
        }

        if( !localizacionBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe localizacion con ese id',
                errors: { message: 'No existe localizacion con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            localizacionBorrado
        });
    });
});

module.exports = app;