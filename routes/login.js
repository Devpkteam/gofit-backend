var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;

var app = express();

var Usuario = require('../models/usuario');

var mdAutenticacion = require ('../middlewares/autenticacion');

app.get('/renuevatoken' ,mdAutenticacion.verificaToken, ( req, res ) => {
    var token = jwt.sign({ usuario: req.usuario }, SEED,{ expiresIn: 14400 }) // cuatro horas
    return res.status(200).json({
        ok: true,
        token
    });
})

//Autenticacion normal

app.post('/', (req, res)=>{

    var body = req.body;

    Usuario.findOne({ email: body.email }, ( err , usuarioBD)=>{

        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar usuarios',
                errors: err
            });
        }

        if (!usuarioBD) {
            return res.status(400).json({
                ok: false,
                msj: 'Credenciales incorrectos',
                errors: err
            });
        }
        
        if ( !bcrypt.compareSync(body.password, usuarioBD.password) ) {
            return res.status(400).json({
                ok: false,
                msj: 'Credenciales incorrectos',
                errors: err
            });
        }

        // Crear un token!!!
        usuarioBD.password = ':)';
        var token = jwt.sign({ usuario: usuarioBD }, SEED,{ expiresIn: 14400 }) // cuatro horas

        res.status(200).json({
            ok: true,
            usuario: usuarioBD,
            token: token,
            id: usuarioBD.id,
            menu: obtenerMenu( usuarioBD.role )
        });
    });
});

function obtenerMenu ( ROLE ) {
    var menu = [
        {titulo: 'Dashboard', url: '/dashboard'},
        {titulo: 'New localitation', url: '/localizacion',},
        {titulo: 'Benchmark', url: '/benchmark'},
        {titulo: 'Best cases', url: '/bestcases'},
        {titulo: 'Pipline', url: '/pipline'},
        {titulo: 'Portfolio', url: '/portfolio'},
    ];

      if ( ROLE === 'ADMIN_ROLE' ) {
        menu.push( 
            {
                titulo: 'Config',
                submenu: [
                    { titulo: 'Gestión de países', url: '/paises' },
                    { titulo: 'Usuarios', url: '/usuarios' },
                ]
            }
         )
      }  

    return menu;
}

module.exports = app;