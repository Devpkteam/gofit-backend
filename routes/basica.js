var express = require('express');
var mdAutenticacion = require('../middlewares/autenticacion');

var Basica = require('../models/basica');

var app = express();

app.get('/',( req, res ) => {
    Basica.find({  })
    .exec(
        ( err, basica ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando basica',
                    errors: err
                });
            } 
            Basica.count({}, (err, conteo) =>{
            res.status(200).json({
                ok: true,
                basica,
                total: conteo
            });
        });
    });
});

app.post('/', mdAutenticacion.verificaToken,( req, res ) => {
    var body = req.body;
    var basica = new Basica({
        nombre: body.nombre,
        usuario: body.usuario,
        nombre_comercio: body.nombre_comercio,
        direccion: body.direccion,
        ciudad: body.ciudad,
        agente: body.agente,
        fecha: body.fecha,
        pais: body.pais,
        supervisor: body.supervisor,
        velocidadCierre: body.velocidadCierre,
        codigo: body.codigo,
        analisisNumerico: body.analisisNumerico,
        anio: body.anio,
        estatus: body.estatus,
        fechaEntrega: body.fechaEntrega,
        penetracion: body.penetracion,
        tipoAnalisis: body.tipoAnalisis,
        miembroEstimado: body.miembroEstimado,
        contratos: body.contratos,
        renta: body.renta,
        capex: body.capex,
        openingYear: body.openingYear,
        ebita: body.ebita,
        payback: body.payback,
        expancion: body.expancion,
        construccion: body.construccion,
        operacion: body.operacion,
        cfo: body.cfo,
        other: body.other,
    });

    console.log(basica);
    
    
    basica.save( ( err, basicaGuardado ) => {
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al guardar informacion basica',
                err
            });
        }
        res.status(201).json({
            ok: true,
            basicaGuardado,
        });
    });
});

app.put('/:id', mdAutenticacion.verificaToken,( req, res ) => {
    var id = req.params.id;
    var body = req.body;

    Basica.findById(id, (err, basica) => {

        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al buscar informacion basica',
                errors: err
            });
        }
    
        if (!basica) {
            return res.status(400).json({
                ok: false,
                msj: 'la informacion basica con el id '+ id + ' no existe',
                errors: { message: 'No existe una informacion basica con ese ID' }
            });
        }

        basica.nombre = body.nombre,
        basica.usuario = body.usuario,
        basica.nombre_comercio = body.nombre_comercio,
        basica.direccion = body.direccion,
        basica.ciudad = body.ciudad,
        basica.agente = body.agente,
        basica.fecha = body.fecha,
        basica.pais = body.pais,
        basica.supervisor = body.supervisor,
        basica.velocidadCierre = body.velocidadCierre,
        basica.codigo = body.codigo,
        basica.analisisNumerico = body.analisisNumerico,
        basica.anio = body.anio,
        basica.estatus = body.estatus,
        basica.fechaEntrega = body.fechaEntrega,
        basica.penetracion = body.penetracion,
        basica.tipoAnalisis = body.tipoAnalisis,
        basica.miembroEstimado = body.miembroEstimado,
        basica.contratos = body.contratos,
        basica.renta = body.renta,
        basica.capex = body.capex,
        basica.OpeningYear = body.OpeningYear,
        basica.ebita = body.ebita,
        basica.payback = body.payback,
        basica.expancion = body.expancion,
        basica.construccion = body.construccion,
        basica.operacion = body.operacion,
        basica.cfo = body.cfo,
        basica.other = body.other,        

        basica.save( (err, basicaGuardado) => {

            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    msj: 'Error al actualizar basica',
                    errors: err
                });
            }

            res.status(201).json({
                ok: true,
                basicaGuardado
            });
        });
    });
});

app.delete('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE] ,( req, res ) => {
    var id = req.params.id;
    Basica.findByIdAndRemove(id, (err, basicaBorrado) => { 
        if( err ) {
            return res.status(500).json({
                ok: false,
                msj: 'Error al borrar informacion basica',
                errors: err
            });
        }

        if( !basicaBorrado ) {
            return res.status(500).json({
                ok: false,
                msj: 'No existe informacion basica con ese id',
                errors: { message: 'No existe informacion basica con ese id'}
            });
        }

        res.status(200).json({
            ok: true,
            basicaBorrado
        });
    });
});

module.exports = app;
