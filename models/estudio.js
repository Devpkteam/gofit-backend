var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var estudioShema =	new Schema({
    competition: { type: Number, required: [true, 'El nombre	es	competition'], match: [/^[0-9]$/, 'la competition debe tener numeros']},
    visibility: { type: Number, required: [true, 'Es necesario la visibility'], match: [/^[0-9]$/, 'la visibility debe tener numeros'] },
    access: {	type: Number, required: [true, 'Es necesario la access'], match: [/^[0-9]$/, 'la access debe tener numeros'] },
    date: {	type: Number, required: [true, 'Es necesario el date'], match: [/^[0-9]$/, 'la date debe tener numeros']},
    action: { type: String,	required: [true, 'Es necesario la action'],match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el action solo admite letras.']  },
    risk: {	type: String, required: [true, 'Es necesario el risk'],match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el risk solo admite letras.'] },
    supervisor: { type: String, required: [true, 'Es necesario el supervisor'],match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el supervisor solo admite letras.']  },
    status: { type: String,	required: [true, 'Es necesario la status'],match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el status solo admite letras.'] },
    sign: {	type: Number, required:[true, 'Es necesario la sign'], match: [/^[0-9]$/, 'la sign debe tener numeros'] },
    open: {	type: Number, required: [true, 'Es necesario la open'], match: [/^[0-9]$/, 'la open debe tener numeros'] },
    addToPortfolio: { type: Boolean, required: [true, 'Es necesario la addToPortfolio']},
    addToBestCases: { type: Boolean, required: [true, 'Es necesario el addToBestCases'] },
    addToPipeline: { type: Boolean, required: [true, 'Es necesario la addToPipeline']},
});

module.exports = mongoose.model('estudio', estudioShema);