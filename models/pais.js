var mongoose =	require('mongoose');
var Schema =	mongoose.Schema;
var paisSchema =	new Schema({
    nombre: {	type: String,	required: [true,	'El	nombre	es	necesario'], match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el nombre solo admite letras.']	},
    img: {	type: String,	required: false },
    usuario: {	type: Schema.Types.ObjectId,	ref: 'Usuario',	required: true },
});

module.exports = mongoose.model('Pais',	paisSchema);