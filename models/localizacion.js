var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var localizacionShema =	new Schema({
    public: { type: Boolean, required: [true, 'El nombre	es	public']},
    building: { type: Boolean, required: [true, 'Es necesario la building'] },
    private: {	type: Boolean, required: [true, 'Es necesario la private'] },
    concurso: {	type: Boolean, required: [true, 'Es necesario el concurso']},
    bilateral: { type: Boolean,	required: [true, 'Es necesario la bilateral'] },
    plot: {	type: Boolean, required: [true, 'Es necesario el plot']},
    demolition: { type: Boolean, required: true },
    urbanized: { type: Boolean,	required: [true, 'Es necesario la velocidad de cierre']},
    sizePlot: {	type: Number, required: [false] },
    sizeBuilding: {	type: Number, required: [false]  },
    volume: { type: Number,	required: [false] },
    walkTo: { type: Boolean, required: [true, 'Es necesario el walkTo'] },
    parking: { type: Number, required: [false] },
    speaker: { type: Boolean, required: [true, 'Es necesario la fecha de entrega'], match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el pais solo admite speaker.']  },
    driveTo: { type: Boolean, required: [true, 'Es necesario la penetracion'] },
    direccionMapa: { type: String,	required: [true, 'Es necesario el tipo de analisis'] },
});

module.exports = mongoose.model('localizacion', localizacionShema);