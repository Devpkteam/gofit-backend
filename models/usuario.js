var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = mongoose.Schema;


var rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol permitido'
};


var usuarioSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'], match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el nombre solo admite letras.'] },
    apellido: { type: String, required: [true, 'El apellido es necesario'], match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el apellido solo admite letras.'] },
    email: { type: String, unique: true, required: [true, 'El correo es necesario'], match: [/\S+@\S+\.\S+/, 'el email no tiene formato correo.'] },
    password: { type: String, required: [true, 'La contraseña es necesaria'] },
    tlf: { type: Number, required: false },
    img: { type: String, required: false },
    role: { type: String, required: true, default: 'USER_ROLE', enum: rolesValidos },
    permisos: { type: String},
});

usuarioSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Usuario', usuarioSchema);