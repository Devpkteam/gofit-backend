var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var basicaSchema =	new Schema({
    nombre_comercio: {	type: String,	required: [true,	'El	nombre	es	necesario'], match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el nombre solo admite letras.']	},
    direccion: { type: String,	required: [true, 'Es necesario la direccion'] },
    ciudad: {	type: String,	required: [true, 'Es necesario la ciudad'] },
    agente: {	type: String,	required: [true, 'Es necesario el agente'],match: [/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/, 'el nombre de agente solo admite letras.'] },
    fecha: {	type: Date,	required: [true, 'Es necesario la fecha'] },
    pais: {	type: String,	required: [true, 'Es necesario el pais'] },
    supervisor: {	type: String,	required: true },
    velocidadCierre: {	type: Number,	required: [true, 'Es necesario la velocidad de cierre'], match: [/^[0-9]$/, 'la velocidad debe tener numeros'] },
    codigo: {	type: String,	required: [true, 'Es necesario el codigo'] },
    usuario: {	type: Schema.Types.ObjectId,	ref: 'Usuario',	required: true },
    analisisNumerico: {	type: String,	required: [true, 'Es necesario el codigo'] },
    anio: {	type: Date,	required: [true, 'Es necesario el anio'] },
    estatus: {	type: String,	required: [true, 'Es necesario el estatus'] },
    fechaEntrega: {	type: Date,	required: [true, 'Es necesario la fecha de entrega'] },
    penetracion: {	type: Number,	required: [true, 'Es necesario la penetracion'] },
    tipoAnalisis: {	type: String,	required: [true, 'Es necesario el tipo de analisis'] },
    miembroEstimado: {	type: Number,	required: [true, 'Es necesario cantidad de miembros'] },
    payback: {	type: Number,	required: [true, 'Es necesario cantidad de payback'] },
    openingYear: {	type: Number, required: [true, 'Es necesario cantidad de OpeningYear'], match: [/^[0-9]$/, 'la opening debe tener numeros'] },
    ebita: { type: Number,	required: [true, 'Es necesario cantidad de ebita'] },
    contratos: { type: Number,	required: [true, 'Es necesario cantidad de contratos'] },
    expancion: { type: Boolean},
    construccion: { type: Boolean},
    operacion: { type: Boolean},
    cfo: {	type: Boolean},
    other: { type: Boolean},
});

module.exports = mongoose.model('basica', basicaSchema);
